package io.olbrich.secretsanta.matchmaker.api

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.lang.Exception

@ResponseStatus(code= HttpStatus.NOT_FOUND)
class NotFoundException: Exception("The requested party could not be found.")
