package io.olbrich.secretsanta.matchmaker.api

import javax.persistence.Entity

@Entity
data class Person(
        val firstName: String,
        val lastName: String
) {
    override fun toString(): String {
        return "$firstName $lastName"
    }
}