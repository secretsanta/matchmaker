package io.olbrich.secretsanta.matchmaker.api

data class GiftExchange(val giver:Person, val recipient:Person) {
    override fun toString(): String {
        return "$giver -> $recipient"
    }
}