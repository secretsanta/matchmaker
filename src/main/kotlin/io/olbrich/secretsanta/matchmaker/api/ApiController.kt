package io.olbrich.secretsanta.matchmaker.api

import io.olbrich.secretsanta.matchmaker.matching.MaximumFlowFinder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * Main RESTful entrypoint for the application.
 */
@CrossOrigin
@RestController()
class ApiController(@Autowired val secretSantaPartyRepository: FakeSecretSantaPartyRepository,
                    @Autowired val maximumFlowFinder: MaximumFlowFinder) {

    @GetMapping("api/party")
    fun getParties(): List<SecretSantaParty> {
        return secretSantaPartyRepository.findAll()
    }

    @GetMapping("api/party/{id}")
    fun getParty(@PathVariable id: String): SecretSantaParty {
        return secretSantaPartyRepository.findOneById(id)
    }

    @PostMapping("api/party")
    fun createParty(@RequestBody party: SecretSantaParty): SecretSantaParty {
        return secretSantaPartyRepository.save(party)
    }

    @PutMapping("api/party")
    fun updateParty(@RequestBody party: SecretSantaParty): SecretSantaParty {
        return secretSantaPartyRepository.update(party)
    }

    @DeleteMapping("api/party/{id}")
    fun deleteParty(@PathVariable id: String) {
        val party = secretSantaPartyRepository.findOneById(id)
        secretSantaPartyRepository.delete(party)

        return
    }

    @GetMapping("api/party/{id}/matches")
    fun getMatches(@PathVariable id: String): List<GiftExchange> {
        val party = secretSantaPartyRepository.findOneById(id)

        return maximumFlowFinder.getMatches(party.people, party.exclusionGroups)
    }

    @DeleteMapping("purge")
    fun purge() {
        secretSantaPartyRepository.deleteAll()
        return
    }
}
