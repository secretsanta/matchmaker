package io.olbrich.secretsanta.matchmaker.api

import javax.persistence.*

@Entity
data class SecretSantaParty (
        val name: String,
        val people: List<Person>,
        val exclusionGroups: List<ExclusionGroup>
) {
        @Id
        lateinit var id: String
}
