package io.olbrich.secretsanta.matchmaker.api

import org.springframework.stereotype.Service
import java.util.*

@Service
class FakeSecretSantaPartyRepository {

    val parties = HashMap<String, SecretSantaParty>()

    init {
        addTestData()
    }


    fun findOneById(id:String): SecretSantaParty {
        if (! parties.containsKey(id)) throw NotFoundException()

        return parties[id]!!
    }

    fun findAll(): List<SecretSantaParty> {
        return parties.values.toList()
    }

    fun save(party: SecretSantaParty): SecretSantaParty {
        val id = Random().nextLong().toString()
        party.id = id
        parties[id] = party

        return party
    }

    fun update(party: SecretSantaParty): SecretSantaParty {
        parties[party.id] = party
        return party
    }

    fun delete(party: SecretSantaParty) {
        parties.remove(party.id)
    }

    fun deleteAll() {
        parties.clear()
        addTestData()
    }

    private fun addTestData() {
        addFireflyTestData()
        addSimpleNoExclusionTestData()
        addSimpleNoPerfectMatchTestData()
    }

    private fun addFireflyTestData() {
        val people = ArrayList<Person>()
        people.add(Person("Mal", "Reynolds"))
        people.add(Person("Zoe", "Washburne"))
        people.add(Person("Wash", "Washburne"))
        people.add(Person("Inara", "Serra"))
        people.add(Person("Jayne", "Cobb"))
        people.add(Person("Kaylee", "Frye"))
        people.add(Person("Simon", "Tam"))
        people.add(Person("River", "Tam"))
        people.add(Person("Derrial", "Book"))

        val exclusions = ArrayList<ExclusionGroup>()
        exclusions.add(ExclusionGroup(arrayListOf(1,2)))
        exclusions.add(ExclusionGroup(arrayListOf(5,6)))
        exclusions.add(ExclusionGroup(arrayListOf(6,7)))

        val party = SecretSantaParty("Firefly", people, exclusions)
        party.id = "1"

        this.update(party)
    }


    private fun addSimpleNoExclusionTestData() {
        val people = ArrayList<Person>()
        people.add(Person("Alice", "A."))
        people.add(Person("Bob", "B."))
        people.add(Person("Charlie", "C."))

        val exclusions = ArrayList<ExclusionGroup>()

        val party = SecretSantaParty("Simple No Exclusion", people, exclusions)
        party.id = "2"

        this.update(party)
    }

    private fun addSimpleNoPerfectMatchTestData() {
        val people = ArrayList<Person>()
        people.add(Person("Alice", "A."))
        people.add(Person("Bob", "B."))
        people.add(Person("Charlie", "C."))

        val exclusions = ArrayList<ExclusionGroup>()
        exclusions.add(ExclusionGroup(arrayListOf(1, 2)))

        val party = SecretSantaParty("Simple No Perfect Match", people, exclusions)
        party.id = "3"

        this.update(party)
    }
}
