package io.olbrich.secretsanta.matchmaker.matching.graph

import io.olbrich.secretsanta.matchmaker.api.Person

class EdgeValidator {

    fun isValidExchange(giver: Vertex, receiver: Vertex, exclusions: List<List<Person>>) =
            isGiverReceiver(giver, receiver) && !isPairingIsExcluded(giver, receiver, exclusions)

    private fun isGiverReceiver(giver: Vertex, receiver: Vertex) =
            giver.person != receiver.person

    private fun isPairingIsExcluded(giver: Vertex, receiver: Vertex, exclusions: List<List<Person>>): Boolean {
        var isExcluded = false

        for (exclusion in exclusions) {
            if (exclusion.containsAll(arrayListOf(giver.person, receiver.person))) {
                isExcluded = true
            }
        }
        return isExcluded
    }
}
