package io.olbrich.secretsanta.matchmaker.matching.graph

enum class VertexType{
    SOURCE, SINK, GIVER, RECEIVER
}