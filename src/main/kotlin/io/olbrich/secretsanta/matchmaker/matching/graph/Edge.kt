package io.olbrich.secretsanta.matchmaker.matching.graph

import io.olbrich.secretsanta.matchmaker.api.GiftExchange
import org.jgrapht.graph.DefaultEdge

class Edge: DefaultEdge() {

    public override fun getSource(): Any {
        return super.getSource()
    }

    public override fun getTarget(): Any {
        return super.getTarget()
    }

    fun asGiftExchange(): GiftExchange {
        return GiftExchange(
                (source as Vertex).person!!,
                (target as Vertex).person!!
        )
    }
}