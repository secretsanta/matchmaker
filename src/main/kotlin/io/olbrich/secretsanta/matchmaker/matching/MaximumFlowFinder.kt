package io.olbrich.secretsanta.matchmaker.matching

import io.olbrich.secretsanta.matchmaker.api.GiftExchange
import io.olbrich.secretsanta.matchmaker.api.ExclusionGroup
import io.olbrich.secretsanta.matchmaker.api.Person

interface MaximumFlowFinder {
    fun getMatches(people: List<Person>, exclusions: List<ExclusionGroup>): List<GiftExchange>
}