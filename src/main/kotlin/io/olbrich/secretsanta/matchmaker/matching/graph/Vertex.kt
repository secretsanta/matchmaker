package io.olbrich.secretsanta.matchmaker.matching.graph

import io.olbrich.secretsanta.matchmaker.api.Person

data class Vertex(val type: VertexType, val person: Person?)