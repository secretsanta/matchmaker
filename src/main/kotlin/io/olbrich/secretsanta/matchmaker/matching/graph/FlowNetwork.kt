package io.olbrich.secretsanta.matchmaker.matching.graph

import io.olbrich.secretsanta.matchmaker.api.ExclusionGroup
import io.olbrich.secretsanta.matchmaker.api.Person
import org.jgrapht.graph.DefaultDirectedGraph
import java.util.*

class FlowNetwork(people: List<Person>, exclusions: List<ExclusionGroup>) {
    val flowNetwork = DefaultDirectedGraph<Vertex, Edge>(Edge::class.java)
    val source = Vertex(VertexType.SOURCE, null)
    val sink = Vertex(VertexType.SINK, null)

    private val givers = ArrayList<Vertex>()
    private val receivers = ArrayList<Vertex>()
    private val exchangeValidator = EdgeValidator()

    init {
        // Add source, sink, and all of the giver/receiver vertices
        flowNetwork.addVertex(source)
        flowNetwork.addVertex(sink)

        addGivers(people)
        addReceivers(people)

        // Connect givers to receivers they can give to
        for (giver in givers) {
            for (receiver in receivers) {
                // Map list to their people
                val exclusionsAsPeople = ArrayList<List<Person>>()

                for (exclusionGroup in exclusions) {
                    exclusionsAsPeople.add(exclusionGroup.peopleIndexes.map { index -> people[index] }.toList())
                }

                if(exchangeValidator.isValidExchange(giver, receiver, exclusionsAsPeople)) {
                    flowNetwork.addEdge(giver, receiver)
                }
            }
        }

        // Connect givers to source and receivers to sink
        for (giver in givers) flowNetwork.addEdge(source, giver)
        for (receiver in receivers) flowNetwork.addEdge(receiver, sink)
    }

    private fun addReceivers(people: List<Person>) {
        for (person in people.shuffled(Random(5678))) {
            val receiver = Vertex(VertexType.RECEIVER, person)

            flowNetwork.addVertex(receiver)
            receivers.add(receiver)
        }
    }

    private fun addGivers(people: List<Person>) {
        for (person in people.shuffled(Random(1234))) {
            val giver = Vertex(VertexType.GIVER, person)

            flowNetwork.addVertex(giver)
            givers.add(giver)
        }
    }


}
