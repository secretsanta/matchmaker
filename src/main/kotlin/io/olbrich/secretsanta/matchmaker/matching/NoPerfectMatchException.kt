package io.olbrich.secretsanta.matchmaker.matching

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.lang.Exception

@ResponseStatus(code= HttpStatus.BAD_REQUEST)
class NoPerfectMatchException: Exception("A perfect match can not be created for this party.")
