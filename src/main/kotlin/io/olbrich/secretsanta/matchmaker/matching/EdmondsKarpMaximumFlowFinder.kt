package io.olbrich.secretsanta.matchmaker.matching

import io.olbrich.secretsanta.matchmaker.api.GiftExchange
import io.olbrich.secretsanta.matchmaker.api.ExclusionGroup
import io.olbrich.secretsanta.matchmaker.api.Person
import io.olbrich.secretsanta.matchmaker.matching.graph.FlowNetwork
import io.olbrich.secretsanta.matchmaker.matching.graph.Vertex
import io.olbrich.secretsanta.matchmaker.matching.graph.VertexType
import org.jgrapht.alg.flow.EdmondsKarpMFImpl
import org.springframework.stereotype.Service
import kotlin.streams.toList

@Service
class EdmondsKarpMaximumFlowFinder: MaximumFlowFinder {

    override fun getMatches(people: List<Person>, exclusions: List<ExclusionGroup>): List<GiftExchange> {
        val flowNetwork = FlowNetwork(people, exclusions)

        val flowMap = EdmondsKarpMFImpl(flowNetwork.flowNetwork).getMaximumFlow(flowNetwork.source, flowNetwork.sink).flowMap

        val giftExchangeEdges = flowMap.keys.stream()
                .filter{ key -> flowMap[key]!! > 0 }
                .filter{ key -> (key.source as Vertex).type == VertexType.GIVER}
                .toList()

        if(giftExchangeEdges.size != people.size) throw NoPerfectMatchException()

        return giftExchangeEdges.map { edge -> edge.asGiftExchange() }
    }


}
