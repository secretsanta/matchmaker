package io.olbrich.secretsanta.matchmaker.matching

import io.olbrich.secretsanta.matchmaker.api.ExclusionGroup
import io.olbrich.secretsanta.matchmaker.api.Person
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.*

internal class EdmondsKarpMaximumFlowFinderTest {

    @org.junit.jupiter.api.Test
    fun perfectMatch() {
        val a1 = Person("a", "1")
        val b1 = Person("b", "1")
        val c1 = Person("c", "1")
        val d2 = Person("d", "2")
        val e2 = Person("e", "2")
        val f3 = Person("f", "3")

        val people = arrayListOf(a1, b1, c1, d2, e2, f3)

        val exclusion1 = ExclusionGroup(arrayListOf(0, 1, 2))
        val exclusion2 = ExclusionGroup(arrayListOf(3, 4))
        val exclusions = arrayListOf(exclusion1, exclusion2)

        val res = EdmondsKarpMaximumFlowFinder().getMatches(people, exclusions)

        assertThat(res.size).isEqualTo(people.size)

        // Exclusions happen to match with last name
        res.forEach{exchange ->
            assertThat(exchange.giver.lastName != exchange.recipient.lastName)
        }
    }

    @org.junit.jupiter.api.Test
    fun imperfectMatch() {
        val a1 = Person("John", "Smith")
        val b1 = Person("Jane", "Smith")
        val c1 = Person("Bob", "Doe")

        val people = arrayListOf(a1, b1, c1)

        val exclusion = ExclusionGroup(arrayListOf(1, 2))
        val exclusions = arrayListOf(exclusion)

        assertThrows(NoPerfectMatchException::class.java) {
            EdmondsKarpMaximumFlowFinder().getMatches(people, exclusions)
        }
    }
}
