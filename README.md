# Secret Santa Server

This is the server application for the Secret Santa programming challenge. It is a Spring Boot 2.1.1 based server 
written in Kotlin. It has been designed to deploy to GKE via GitLab's Auto DevOps. It can also be run locally via 
docker.

## Assumptions and Limitations

* Only rudimentary error checking is included. It's assumed that the user won't actively be trying to break the app.
* There is no true persistence layer. Data is stored in-memory but may be purged at any time.
* There is no authentication mechanism. 

## Matching Algorithm

This problem can be 
[reduced to a network flow problem](https://www.cs.cmu.edu/~ckingsf/bioinfo-lectures/matching.pdf). To solve for the
maximum flow, I used `jgrapht`'s implementation of the Edmonds-Karp algorithm. Because we're interested in the perfect
maximum flow where a valid gift giving exchange exists for all party members, I return a `NoPerfectMatchException` if 
the party doesn't have a perfect match.

I initially had issues with the application usually matching two people with each other. Given additional time, I would
modify the Edmonds-Karp algorithm to discourage these matches. To help prevent this in the current implementation, I 
randomize the vertex creation order in `FlowNetwork`.

## API

The application logic is accessed through a simple API that provides CRUD operations on a `SecretSantaParty` object. 

### Objects

#### `SecretSantaParty`
```
{
    name: string;
    people: Array<Person>;
    exclusionGroups: Array<ExclusionGroup>;
    id: string;
}
```

#### `GiftExchange`
```
{
    giver: Person;
    recipient: Person;
}
```

#### `Person`
```
{
    firstName: string;
    lastName: string;
}
```

#### `ExclusionGroup`
The indexes match to the ordering of the `people` array in `SecretSantaParty`.
```
{
    peopleIndexes: Array<number>;
}
```

### Actions

#### `GET /api/party`
Returns an array of all `SecretSantaParty`

#### `GET /api/party/{id}`
Returns a specific `SecretSantaParty` by an ID. Returns a 404 if the party doesn't exist.

#### `POST /api/party`
Creates a `SecretSantaParty`. The `id` field in the `POST`ed object is optional.

#### `PUT /api/party`
Updates a `SecretSantaParty` specified by the `id` field.

#### `GET /api/party/{id}/matches`
Returns an array of `GiftExchange` for the party with the given `id`.

#### `DELETE /api/party/{id}`
Deletes the `SecretSantaParty` with the given `id`.

#### `DELETE /api/party`
Purges the in-memory store of all `SecretSantaParty` resources, then re-add the test data.


